def main():
    for num in xrange(1,101):
        if num % 5 == 0 and num % 3 == 0:
            print "fizzbuzz"
        elif num % 3 == 0:
            print "fizz"
        elif num % 5 == 0:
            print "buzz"
        else:
            print num


if __name__ == "main":
    main()